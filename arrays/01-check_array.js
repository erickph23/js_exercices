function checkIfArray(value) {
  if (Array.isArray(value)) {
    console.log('It is an Array')
    return
  }
  console.log('It is not an Array')
}

checkIfArray('el tiro')
checkIfArray(['azul', 'rojo', 'verde'])
checkIfArray([])
